import React, { useState, useEffect } from 'react';
import './App.css';

type Report = {
  duration: Duration;
};
type Record = {
  id: number;
  text: string;
  start: string;
  end: string;
  duration: Duration;
};

type Duration = {
  days?: number;
  hours?: number;
  minutes?: number;
};

function formatDate(date: string) {
  return new Date(date).toLocaleString();
}
function formatDuration(duration: Duration) {
  let str = '';
  if (duration.days) {
    str = str + ' ' + duration.days + ' days';
  }
  if (duration.hours) {
    str = str + ' ' + duration.hours + ' hours';
  }
  if (duration.minutes) {
    str = str + ' ' + duration.minutes + ' minutes';
  }
  return str;
}

let MS = 1;
let SECOND = MS * 1000;
let MINUTE = SECOND * 60;
let HOUR = MINUTE * 60;
let DAY = HOUR * 24;

let MONTHS: { text: string; value: number }[] = [];

for (let i = 0; i < 12; i++) {
  let date = new Date();
  date.setMonth(i, 1);
  date.setHours(0, 0, 0, 0);
  let text = new Intl.DateTimeFormat('en-HK', { dateStyle: 'long' } as any)
    .format(date)
    .split(' ')[1];
  MONTHS.push({ text, value: i });
}

function App() {
  const [text, setText] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');

  const [searchStartDate, setSearchStartDate] = useState('');
  const [searchEndDate, setSearchEndDate] = useState('');
  const [searchMonth, setSearchMonth] = useState('');
  const [searchYear, setSearchYear] = useState(new Date().getFullYear());

  const [records, setRecords] = useState<Record[]>([]);
  const [report, setReport] = useState<Report | null>(null);

  async function loadRecords() {
    let res = await fetch('http://localhost:8090/record/all');
    let json = await res.json();
    setRecords(json.records);
  }

  useEffect(() => {
    loadRecords();
  }, []);

  console.log({ text, startDate, startTime, endDate, endTime, records });

  async function search() {
    let start = new Date(searchStartDate + ' 00:00');
    let end = new Date(searchEndDate + ' 00:00');
    end.setTime(end.getTime() + DAY);
    let startStr = start.toISOString();
    let endStr = end.toISOString();

    let res = await fetch(
      `http://localhost:8090/record?start=${startStr}&end=${endStr}&text=${text}`,
    );
    let json = await res.json();
    setRecords(json.records);
  }

  async function loadReport() {
    let start = new Date(searchStartDate + ' 00:00');
    let end = new Date(searchEndDate + ' 00:00');
    end.setTime(end.getTime() + DAY);
    let startStr = start.toISOString();
    let endStr = end.toISOString();

    let res = await fetch(
      `http://localhost:8090/record/report?start=${startStr}&end=${endStr}&text=${text}`,
    );
    let json = await res.json();
    setReport(json.report);
  }

  function d2(d: number) {
    if (d < 10) {
      return '0' + d;
    } else {
      return d.toString();
    }
  }

  async function send() {
    let start = new Date(startDate + ' ' + startTime);
    let end = new Date(endDate + ' ' + endTime);
    await fetch('http://localhost:8090/record', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        text,
        start: start.toISOString(),
        end: end.toISOString(),
      }),
    });
    await loadRecords();
  }
  return (
    <div className="App">
      <h2>Create Record</h2>
      <h3>start</h3>
      <input
        type="date"
        value={startDate}
        onChange={(e) => setStartDate(e.target.value)}
      />
      <input
        type="time"
        value={startTime}
        onChange={(e) => setStartTime(e.target.value)}
      />
      <h3>end</h3>
      <input
        type="date"
        value={endDate}
        onChange={(e) => setEndDate(e.target.value)}
      />
      <input
        type="time"
        value={endTime}
        onChange={(e) => setEndTime(e.target.value)}
      />
      <h3>text</h3>
      <input
        type="text"
        value={text}
        onChange={(e) => setText(e.target.value)}
      />
      <h3></h3>
      <button onClick={send}>send</button>
      <h2>Search Record</h2>
      <input
        type="number"
        value={searchYear}
        onChange={(e) => setSearchYear(e.target.valueAsNumber)}
      />
      <select
        value={searchMonth}
        onChange={(e) => {
          setSearchMonth(e.target.value);

          let start = new Date();
          start.setFullYear(searchYear)
          start.setMonth(+e.target.value, 1);
          start.setHours(0, 0, 0, 0);

          let end = new Date();
          end.setFullYear(searchYear)
          end.setMonth(+e.target.value + 1, 0);
          end.setHours(0, 0, 0, 0);

          setSearchStartDate(
            start.getFullYear() +
              '-' +
              d2(start.getMonth() + 1) +
              '-' +
              d2(start.getDate()),
          );

          setSearchEndDate(
            end.getFullYear() +
              '-' +
              d2(end.getMonth() + 1) +
              '-' +
              d2(end.getDate()),
          );
        }}
      >
        <option value="">Please select a month</option>
        {MONTHS.map((option) => (
          <option key={option.value} value={option.value}>
            {option.text}
          </option>
        ))}
      </select>
      <br/>
      <input
        type="date"
        value={searchStartDate}
        onChange={(e) => setSearchStartDate(e.target.value)}
      />
      <input
        type="date"
        value={searchEndDate}
        onChange={(e) => setSearchEndDate(e.target.value)}
      />
      <br />
      <button onClick={search}>search</button>
      <button onClick={loadReport}>report</button>
      <h2>Records</h2>
      <table>
        <thead>
          <tr>
            <th>id</th>
            <th>text</th>
            <th>start</th>
            <th>end</th>
            <th>duration</th>
          </tr>
        </thead>
        <tbody>
          {records.map((record) => (
            <tr key={record.id}>
              <td>{record.id}</td>
              <td>{record.text}</td>
              <td>{formatDate(record.start)}</td>
              <td>{formatDate(record.end)}</td>
              <td>{formatDuration(record.duration)}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <h2>Report</h2>
      {report ? (
        <div>Total Duration: {JSON.stringify(report.duration)}</div>
      ) : undefined}
    </div>
  );
}

export default App;
