let now = Date.now();

let start = new Date(now).toISOString();
let end = new Date(now + 1000 * 60 * 60 * 3.31).toISOString();
let diff = new Date(end).getTime() - new Date(start).getTime();
console.log({ start, end, diff, duration: formatDuration(diff) });







function formatDuration(duration) {
  if (duration > 1000 * 60 * 60) {
    let ms = duration % 1000;
    duration = duration - ms;

    let second = (duration / 1000) % 60;
    duration = duration - second * 1000;

    let minute = (duration / 1000 / 60) % 60;
    duration = duration - minute * 1000 * 60;

    let hour = duration / 1000 / 60 / 60;
    return `${hour} hr ${minute} min`;
  }
  if (duration > 1000 * 60) {
    return Math.round(duration / 1000 / 60) + ' minutes';
  }
  if (duration > 1000) {
    return Math.round(duration / 1000) + ' seconds';
  }
  return duration + ' ms';
}










function formatDuration2(duration) {
  let reminding = duration;

  let ms = reminding % 1000;
  reminding = (reminding - ms) / 1000;

  let second = reminding % 60;
  reminding = (reminding - second) / 60;

  let minute = reminding % 60;
  reminding = (reminding - minute) / 60;

  let hour = reminding % 60;
  reminding = (reminding - hour) / 60;

  let day = reminding;

  console.log(`${day} day ${hour} hrs ${minute} min`);
}

formatDuration2(1 * 3);
formatDuration2(1000 * 3);
formatDuration2(1000 * 60 * 3);
formatDuration2(1000 * 60 * 60 * 3);
formatDuration2(1000 * 60 * 60 * 24 * 3);
