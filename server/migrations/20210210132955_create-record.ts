import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('record', (table) => {
    table.increments();
    table.string('text');
    table.timestamp('start');
    table.timestamp('end');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('record')
}
