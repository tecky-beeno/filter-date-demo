import express from 'express';
import cors from 'cors';
import Knex from 'knex';
let configs = require('./knexfile');

let knex = Knex(configs.development);

let app = express();

app.use(cors());
app.use(express.json());

app.post('/record', async (req, res) => {
  console.log(req.method, req.url, req.body);
  try {
    await knex('record').insert(req.body);
    res.json('ok');
  } catch (error) {
    console.log('failed to post record', error);
    res.status(500).json('failed');
  }
});

app.get('/record/all', async (req, res) => {
  try {
    let records = await knex('record').select(
      'record.*',
      knex.raw(`"end" - "start" as duration`),
    );
    res.json({ records });
  } catch (error) {
    console.log('failed to post record', error);
    res.status(500).json('failed');
  }
});

app.get('/record/report', async (req, res) => {
  console.log(req.method, req.path, req.query);
  try {
    let report = await knex('record')
      .select(knex.raw(`sum("end" - "start") as duration`))
      .where('start', '>=', req.query.start as string)
      .andWhere('end', '<', req.query.end as string)
      .andWhere('text', '=', req.query.text as string)
      .first();

    res.json({ report });
  } catch (error) {
    console.log('failed to post record', error);
    res.status(500).json('failed');
  }
});

app.get('/record', async (req, res) => {
  console.log(req.method, req.path, req.query);
  try {
    let records = await knex('record')
      .select('record.*', knex.raw(`"end" - "start" as duration`))
      .where('start', '>=', req.query.start as string)
      .andWhere('end', '<', req.query.end as string)
      .andWhere('text', '=', req.query.text as string);

    res.json({ records });
  } catch (error) {
    console.log('failed to post record', error);
    res.status(500).json('failed');
  }
});

app.listen(8090, () => {
  console.log('listening on http://localhost:8090');
});
